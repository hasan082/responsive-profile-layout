# Responsive Profile Layout

A Flutter application showcasing a responsive profile layout with different designs for portrait and landscape modes.

## Description

This Flutter application demonstrates how to create a responsive profile layout that adapts to both portrait and landscape orientations. In portrait mode, all elements are vertically stacked, while in landscape mode, the profile picture is displayed on the left side with the remaining content on the right.

## Screenshots

| Portrait Mode             | Landscape Mode              |
|---------------------------|-----------------------------|
| ![Portrait](portrait.png) | ![Landscape](landscape.png) |

## Features

- Adaptable layout for portrait and landscape orientations
- Profile picture displayed on the left side in landscape mode
- Responsive design that adjusts based on the device screen size

## Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/hasan082/responsive-profile-layout
   ```

2. Change to the project directory:

   ```bash
   cd your_repository
   ```

3. Install the dependencies:

   ```bash
   flutter pub get
   ```

4. Run the application:

   ```bash
   flutter run
   ```

## Dependencies

The project uses the following dependencies:

- flutter
- flutter/material

## Usage

1. Open the project in your preferred Flutter development environment.
2. Navigate to the `lib` directory and open the `main.dart` file.
3. Customize the profile information, such as name, bio, and profile picture URL, in the `PortraitView` and `LandscapeView` widgets.
4. Run the application in your preferred emulator or device to see the responsive profile layout in action.

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvement, please create an issue or submit a pull request.

## License

This project is licensed under the [MIT License](https://opensource.org/license/mit/).

