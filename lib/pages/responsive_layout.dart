import 'package:flutter/material.dart';
import '../widgets/landscape_widget.dart';
import '../widgets/portrait_widget.dart';

class ResponsivePage extends StatelessWidget {
  const ResponsivePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text('Responsive Profile Layout'),
      ),
      body: SafeArea(
        child: OrientationBuilder(
          builder: (BuildContext context, Orientation orientation) {
            if(orientation == Orientation.portrait){
              return const PortraitView();//portrait layout
            }else {
              return const LandscapeView();//landscape layout
            }
          },
        )
      ),
    );
  }
}
