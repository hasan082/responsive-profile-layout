import 'package:flutter/material.dart';

class GridViewWidget extends StatelessWidget {
  const GridViewWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 5,
        mainAxisSpacing: 5,
      ),
      itemCount: 21,
      itemBuilder: (BuildContext context, int index) {
        return Stack(
          children: [
            Image.network(
              'https://picsum.photos/${500 + index}',
              fit: BoxFit.cover,
            ),
            Container(
              color: Colors.black.withOpacity(0.2), // Adjust opacity as needed
            ),
          ],
        );
      },
    );
  }
}
