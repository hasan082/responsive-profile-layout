import 'package:flutter/material.dart';
import 'package:resposive_layout/widgets/profile_img_circle_widget.dart';

import 'gridview_widget.dart';

class LandscapeView extends StatelessWidget {
  const LandscapeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Center(child: CircleImage(imageUrl: 'https://i.ibb.co/Tm2mF1H/Hasan32.jpg')),
            const SizedBox(width: 20),
            Expanded(
              flex: 2,
              child: ListView(
                physics: const BouncingScrollPhysics(),
                children: const [
                  Text(
                    'Md Hasanuzzaman',
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used',
                    style: TextStyle(fontSize: 18),
                  ),
                  SizedBox(height: 20),
                  GridViewWidget(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
