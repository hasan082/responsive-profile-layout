import 'package:flutter/material.dart';
import 'package:resposive_layout/widgets/profile_img_circle_widget.dart';

import 'gridview_widget.dart';

class PortraitView extends StatelessWidget {
  const PortraitView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return ListView(
      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.all(16),
      children: const [
        Center(
          child: CircleImage(imageUrl: 'https://i.ibb.co/Tm2mF1H/Hasan32.jpg'),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          'Md Hasanuzzaman',
          style: TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.w500,
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used',
          style: TextStyle(fontSize: 18),
        ),
        SizedBox(
          height: 20,
        ),
        GridViewWidget(),
      ],
    );
  }
}
