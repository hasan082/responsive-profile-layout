import 'package:flutter/material.dart';
import 'package:resposive_layout/pages/responsive_layout.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Responsive demo',
      theme: ThemeData(
        primarySwatch: Colors.teal
      ),
      home: const ResponsivePage(),
    );
  }
}
